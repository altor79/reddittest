package ru.startandroid.develop.p1201_clickwidget.data.mappers;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

import ru.startandroid.develop.p1201_clickwidget.data.model.reddit.respdata.PostContext;
import ru.startandroid.develop.p1201_clickwidget.data.model.reddit.respdata.Preview;
import ru.startandroid.develop.p1201_clickwidget.domain.IMapper;
import ru.startandroid.develop.p1201_clickwidget.domain.model.Post;

public class RemotePostMapper implements IMapper<PostContext, Post> {
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH");

    @Override
    public Post map(PostContext postContext) {
        Post post = new Post();
        post.setAuthor(postContext.getAuthor());
        //post.setLikes(postContext.getLikes());
                    long timeposthours = postContext.getCreatedUtc();
                    long timenow = System.currentTimeMillis();
                   //timeposthours = simpleDateFormat.format(new Date(timeposthours*1000) + " " + " HOURS AGO");
                    String timeNow = simpleDateFormat.format(new Date(System.currentTimeMillis()));
                    long timeago = timeposthours - timenow;
                    String hoursAgo = simpleDateFormat.format(new Date(timeago)) + " " + "HOURS AGO";
        post.setTime(hoursAgo);

        // Set url
        String url = postContext.getThumbnail();
        post.setUrl(url);

        // Set previe url
        Preview prv = postContext.getPreview();
        String sourceUrl = null;

        if (prv!=null && prv.getImages()!=null && prv.getImages().size()>0) {
            String redditSourceUrl = prv.getImages().get(0).getSource().getUrl();

            if (redditSourceUrl != null) {
                sourceUrl = redditSourceUrl.replace("webp&amp;", "webp&");
            }
        }

        post.setPreviewUrl(sourceUrl);

        return post;
    }
}
