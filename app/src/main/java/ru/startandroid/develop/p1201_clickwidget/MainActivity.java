package ru.startandroid.develop.p1201_clickwidget;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collection;
import ru.startandroid.develop.p1201_clickwidget.data.model.reddit.RedditResponse;
import ru.startandroid.develop.p1201_clickwidget.data.usecases.DeliveryPostManager;
import ru.startandroid.develop.p1201_clickwidget.domain.IDeliveryManager;
import ru.startandroid.develop.p1201_clickwidget.domain.IPostCallback;
import ru.startandroid.develop.p1201_clickwidget.domain.model.Post;
import ru.startandroid.develop.p1201_clickwidget.presentation.adapter.OnItemClickListener;
import ru.startandroid.develop.p1201_clickwidget.presentation.adapter.OnLastItemLimit;
import ru.startandroid.develop.p1201_clickwidget.presentation.adapter.PostAdapter;

public class MainActivity extends Activity {
    private final static String TAG = "Main";
    private RecyclerView recyclerView;

    private IDeliveryManager<Post> deliveryManager;
    private IPostCallback<Collection<Post>> postCallback;
    private PostAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        Log.d(TAG, "**************");

        deliveryManager = new DeliveryPostManager();
        postCallback = new IPostCallback<Collection<Post>>() {
            @Override
            public void onResult(final Collection<Post> posts) {

//                final StringBuilder authorsStringBuilder = new StringBuilder();
//                int i = 0;
//                for (final Post post : posts) {
//                    final boolean isNext = ++i<posts.size();
//                    authorsStringBuilder.append(i).append(": ").append(post.getAuthor());
//                    if (isNext) authorsStringBuilder.append(",");
//                }

                Log.d("TAG", "Posts size: " + posts.size());

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.addPosts(posts);
                    }
                });

            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }
        };

        deliveryManager.setCallback(postCallback);

        findViewById(R.id.btNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deliveryManager.getNext();
            }
        });
        findViewById(R.id.btPrev).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deliveryManager.getPrevious();
            }
        });

        adapter.create();
    }

    private void init() {
        recyclerView = findViewById(R.id.recycler);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new PostAdapter();
        recyclerView.setAdapter(adapter);
        OnItemClickListener<Post> listener = new OnItemClickListener<Post>() {
            @Override
            public void onItemClick(Post item) {
                Log.d(TAG, "Click: " + item);
                sendData(item);
            }
        };
        OnLastItemLimit limitListener = new OnLastItemLimit() {
            private int last = -1;

            @Override
            public void onLasItemLimit(int lastPosition) {
                if (lastPosition > last) {
                    last = lastPosition;
                    Toast.makeText(MainActivity.this, "Last item: " + lastPosition, Toast.LENGTH_SHORT).show();
                    deliveryManager.getNext();
                }
            }

        };
        adapter.setLastPositionListener(limitListener);
        adapter.setItemClickListener(listener);
    }

    private void sendData(Post post) {
        Intent intent = new Intent(this, MainActivity2.class);
        intent.putExtra("url", post.getUrl());
        Log.d("sendData ", "URL " + post.getUrl());
        startActivity(intent);
    }
}
