package ru.startandroid.develop.p1201_clickwidget;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

public class MainActivity2 extends AppCompatActivity {
    private TextView textView2;
    private TextView textView3;
    private ImageView imageView3;
    Button button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        button = findViewById(R.id.button);
        imageView3 = findViewById(R.id.imageView3);

        // textView2.setText(getIntent().getStringExtra("url"));
        Picasso.get().load(getIntent().getStringExtra("url")).into(imageView3);
    }
}
