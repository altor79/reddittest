package ru.startandroid.develop.p1201_clickwidget.presentation.adapter;

public interface OnLastItemLimit {
    void onLasItemLimit(int lastPosition);
}
