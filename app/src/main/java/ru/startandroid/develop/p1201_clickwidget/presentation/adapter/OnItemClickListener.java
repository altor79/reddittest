package ru.startandroid.develop.p1201_clickwidget.presentation.adapter;

public interface OnItemClickListener<T> {
    void onItemClick(T item);
}
