package ru.startandroid.develop.p1201_clickwidget.domain;

public interface IPostCallback<T> {
    void onResult(T result);
    void onError(Exception e);
}
