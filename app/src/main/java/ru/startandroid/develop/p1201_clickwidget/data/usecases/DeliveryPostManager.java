package ru.startandroid.develop.p1201_clickwidget.data.usecases;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.startandroid.develop.p1201_clickwidget.JSONPlaceHolderApi;
import ru.startandroid.develop.p1201_clickwidget.data.mappers.RemotePostMapper;
import ru.startandroid.develop.p1201_clickwidget.data.model.reddit.RedditResponse;
import ru.startandroid.develop.p1201_clickwidget.data.model.reddit.respdata.PostContext;
import ru.startandroid.develop.p1201_clickwidget.domain.IDeliveryManager;
import ru.startandroid.develop.p1201_clickwidget.domain.IMapper;
import ru.startandroid.develop.p1201_clickwidget.domain.IPostCallback;
import ru.startandroid.develop.p1201_clickwidget.domain.model.Post;

public class DeliveryPostManager implements IDeliveryManager<Post> {

    private final String TAG = "DeliveryPostManager";

    private String after;
    private String before;
    private JSONPlaceHolderApi api;
    private IMapper<PostContext, Post> mapper;

    private IPostCallback<Collection<Post>> callback;

    private LinkedList<String> hashList = new LinkedList<>();

    public DeliveryPostManager() {
        init();
    }

    private void init() {
        mapper = new RemotePostMapper();
        hashList.add(null);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl("https://www.reddit.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api = retrofit.create(JSONPlaceHolderApi.class);
    }

    @Override
    public void setCallback(IPostCallback<Collection<Post>> callback) {
        this.callback = callback;
    }

    @Override
    public void getPrevious() {
        delivery(false);
    }

    @Override
    public void getNext() {
        delivery(true);
    }

    private void delivery(final boolean isNext) {
        Call<RedditResponse> call;
        String hash = null;

        if (isNext) {
            hash = hashList.getLast();
        } else {
            if (hashList.size() < 3) {
                hash = null;
            } else {
                hash = hashList.get(hashList.size() - 3);
            }
        }


        if (isNext) {
            call = api.getPost(10, hash, null);
        } else {
            call = api.getPost(10, hash, null);
        }

        call.enqueue(new Callback<RedditResponse>() {
            @Override
            public void onResponse(Call<RedditResponse> call, Response<RedditResponse> response) {
                RedditResponse resp = response.body();

                after = resp.getResponseData().getAfter();
                Log.d(TAG, "after: " + after);
                before = resp.getResponseData().getBefore();
                Log.d(TAG, "before: " + before);

                if (isNext) {
                    hashList.add(after);
//                    if (hashList.size()>3) {
//                        hashList.removeFirst();
//                    }
                } else {
                    if (hashList.size() > 2) {
                        hashList.removeLast();
                    }
                }

                Log.d(TAG, "HashList: " + hashList);

                int size = resp.getResponseData().getChildren().size();

                ArrayList<Post> posts = new ArrayList<>();

                for (int i = 0; i < size; i++) {

                    // Add to response list
                    PostContext post = resp.getResponseData().getChildren().get(i).getData();
                    posts.add(mapper.map(post));
                }

                if (callback != null) {
                    callback.onResult(posts);
                }
            }

            //  Picasso.get().load("http://i.imgur.com/DvpvklR.png").into((Target) imageView);
//                Log.d(TAG, String.valueOf(exp) + "ответ ");
            @Override
            public void onFailure(Call<RedditResponse> call, Throwable t) {
                Log.d(TAG, "OnFailure: " + t.getMessage());
                t.printStackTrace();
                if (callback != null) {
                    callback.onError(new Exception(t.getMessage()));
                }
            }
        });
    }

}
