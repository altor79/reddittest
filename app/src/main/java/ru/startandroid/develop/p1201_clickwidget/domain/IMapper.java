package ru.startandroid.develop.p1201_clickwidget.domain;

public interface IMapper<Input, Output> {
    Output map(Input input);
}
