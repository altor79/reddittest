package ru.startandroid.develop.p1201_clickwidget.domain.model;

public class Post {
    private String time;
    private String url;
    private String previewUrl;
    private String author;
    private int likes;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    @Override
    public String toString() {
        return "Post{" +
                "time='" + time + '\'' +
                ", url='" + url + '\'' +
                ", previewUrl='" + previewUrl + '\'' +
                ", author='" + author + '\'' +
                ", likes=" + likes +
                '}';
    }
}
