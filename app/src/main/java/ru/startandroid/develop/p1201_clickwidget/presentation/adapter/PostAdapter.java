package ru.startandroid.develop.p1201_clickwidget.presentation.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;

import ru.startandroid.develop.p1201_clickwidget.R;
import ru.startandroid.develop.p1201_clickwidget.domain.model.Post;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder> {

    private ArrayList<Post> list = new ArrayList<>();
    private OnItemClickListener<Post> listener;
    private OnLastItemLimit limitListener;
    private RecyclerView recyclerView;

    public void addPosts(Collection<Post> posts) {
        list.addAll(posts);
        notifyItemRangeInserted(list.size()-posts.size(), posts.size());
    }

    public void setItemClickListener(OnItemClickListener<Post> listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(v);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Post post = list.get(position);
        holder.tvAuthor.setText(post.getAuthor());
        holder.tvTime.setText(post.getTime());
        Picasso.get()
                .load(post.getPreviewUrl())
                .resize(120, 120)
                .into(holder.img);
        holder.vgParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener!=null) {
                    listener.onItemClick(post);
                }
            }
        });

        if (position == list.size()-1 && limitListener!=null) {
            limitListener.onLasItemLimit(position);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setLastPositionListener(OnLastItemLimit limitListener) {
        this.limitListener = limitListener;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        this.recyclerView = null;
    }

    public void create() {
        if (list.isEmpty() && limitListener!=null) {
            limitListener.onLasItemLimit(list.size());
        }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvAuthor;
        public TextView tvTime;
        public ImageView img;
        public ViewGroup vgParent;

        public MyViewHolder(View v) {
            super(v);
            vgParent = v.findViewById(R.id.llParent);
            tvAuthor = v.findViewById(R.id.tvAuthor);
            tvTime = v.findViewById(R.id.tvTime);
            img = v.findViewById(R.id.image);
        }
    }


}
