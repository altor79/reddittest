package ru.startandroid.develop.p1201_clickwidget;

import androidx.annotation.Nullable;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.startandroid.develop.p1201_clickwidget.data.model.reddit.RedditResponse;

public interface JSONPlaceHolderApi {
    @GET("/r/all/top/.json")
    Call<RedditResponse> getPost(
            @Query("limit") int limit,
            @Nullable @Query("after") String after,
            @Nullable @Query("before") String before
    );
}
