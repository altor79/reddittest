package ru.startandroid.develop.p1201_clickwidget.data.model.reddit.respdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Child {

    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("data")
    @Expose
    private PostContext data;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public PostContext getData() {
        return data;
    }

    public void setData(PostContext data) {
        this.data = data;
    }
}
