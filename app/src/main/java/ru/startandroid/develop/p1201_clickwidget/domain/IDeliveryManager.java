package ru.startandroid.develop.p1201_clickwidget.domain;

import java.util.Collection;

public interface IDeliveryManager<T> {
    void setCallback(IPostCallback<Collection<T>> callback);
    void getPrevious();
    void getNext();
}
